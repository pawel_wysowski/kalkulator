﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form1 : Form
    {

        int liczba1;
        int liczba2;
        int wynik;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            liczba1 = Int32.Parse(textBox1.Text);
            liczba2 = Int32.Parse(textBox2.Text);

            if (radioButton1.Checked)
                wynik = liczba1 + liczba2;
            if (radioButton2.Checked)
                wynik = liczba1 - liczba2;
            if (radioButton3.Checked)
                wynik = liczba1 * liczba2;
            if (radioButton4.Checked)
                wynik = liczba1 / liczba2;

            textBox3.Text = "";
            textBox3.Text += wynik;

        }
    }
}
